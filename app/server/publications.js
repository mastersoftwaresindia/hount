/*
 * Always publish current users dealershipId
 */
Meteor.publish(null, function () {
  var userId = this.userId,
      fields = {dealershipId: 1}

  return Meteor.users.find({_id:userId}, {fields: fields})
})


/*
 * OWNERS
 */
Meteor.publish('activeOwners', function(){
  return Meteor.users.find({
    'roles': {$in: ['owner']},
    'profile.active': true
  });
})


/*
 * FINANCE MANAGERS
 */
Meteor.publish('financeManagersForOpenDeals', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);
    var busyFMIds = _.pluck(Deals.find({status: 'active'}).fetch(), 'financeManagerId');

    query['roles'] = {$in: ['manager']};
    query['profile.active'] = true;
    query['_id'] = {$nin: busyFMIds};
    return Meteor.users.find(query);
  }
});

Meteor.publish('allFinanceManagers', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);
    query['roles'] = {$in: ['manager']};
    return Meteor.users.find(query);
  }
});

Meteor.publish('activeFinanceManagers', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);

    query['roles'] = {$in: ['manager']};
    query['profile.active'] = true;
    return Meteor.users.find(query);
  }
});


/*
 * SALESPEOPLE
 */
Meteor.publish('allSalespeople', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);
    query['roles'] = {$in: ['salesperson']};
    return Meteor.users.find(query, {sort: {'profile.active': -1, name: 1}});
  }
});

Meteor.publish('activeSalespeople', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);
    query['roles'] = {$in: ['salesperson']};
    query['profile.active'] = true;
    return Meteor.users.find(query, {sort: {'profile.name': 1}});
  }
});

Meteor.publish('salespersonById', function(salespersonId){
  return Meteor.users.find(salespersonId);
});


/*
 * Manufacturer
 */
Meteor.publish('allManufacturer', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){ 
    return Manufactur.find();
  }
});

Meteor.publish('manufacturerById', function(ManufacturId){
  return Manufactur.find(ManufacturId);
});
Meteor.publish('allModels', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){ 
    return model.find();
  }
});

Meteor.publish('modelById', function(modelId){
  return model.find(modelId);
});
/*
 ** all Category
 */
Meteor.publish('allCategories', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){ 
    return Categories.find();
  }
});

Meteor.publish('categoryById', function(categorieid){
  return Categories.find(categorieid);
});

/*
 ** all power soures
 */
Meteor.publish('allPowersources', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){ 
    return Powersources.find();
  }
});

Meteor.publish('PowerById', function(categorieid){
  return Powersources.find(categorieid);
});
/*
 * DEALERSHIPS
 */
var getUserDealershipsQuery = function(user){
  var query = {};
  if (Roles.userIsInRole(user, ['owner'])){
    var dealershipIds = QueryBuilder.getOwnerDealerships(user);
    query._id = { $in: dealershipIds };
  } else if (!Roles.userIsInRole(user, ['admin'])){
    query._id = user.dealershipId;
  }
  return query;
};

Meteor.publish('allDealerships', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = getUserDealershipsQuery(user),
        opts = {sort: {active: -1, name: 1}};
    return Dealerships.find(query, opts);
  }
});

Meteor.publish('activeDealerships', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = getUserDealershipsQuery(user),
        opts = {sort: {name: 1}};

    query.active = true;
    return Dealerships.find(query, opts);
  }
});

Meteor.publish('dealershipById', function(dealershipId){
  return Dealerships.find(dealershipId);
});


/*
 * DEALS
 */
Meteor.publish('dealById', function(id){
  return Deals.find(id);
});

Meteor.publish('dealsByStatus', function(status){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);
    if (_.isString(status)){
      status = [status];
    }
    query.status = { $in: status };
    return Deals.find(query);
  }
});

Meteor.publish('completedDealsForDay', function(start, end){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = QueryBuilder.getQueryForMyDealerships(user);
    query.completedAt = { $gte: start, $lte: end };
    query.status = 'completed';
    return Deals.find(query);
  }
});

Meteor.publish('completedDealsSearch', function(start, end, dealershipId, name){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    query = QueryBuilder.getQueryForCompletedDeals(user, start, end, dealershipId, name);
    return Deals.find(query);
  }
});


/*
 * USERS
 */
var getUsersQuery = function(user){
  var query = {};
  if (Roles.userIsInRole(user, ['admin', 'simpleuser'])){

    query.dealershipId = user.dealershipId;
    query.roles = {$nin: ['admin', 'owner']};

  } else if (Roles.userIsInRole(user, ['owner'])){

    var dealershipIds = QueryBuilder.getOwnerDealerships(user);
    query.dealershipId = { $in: dealershipIds };
    query.roles = { $nin: ['admin'] };
  }
  return query;
}

Meteor.publish('users', function(){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = getUsersQuery(user);
    return Meteor.users.find();
  }
});

Meteor.publish('userById', function(userId){
  var user = Meteor.users.findOne(this.userId);
  if (user){
    var query = getUsersQuery(user);
    query._id = userId;
    return Meteor.users.find();
  }
});


/*
 * REPORTS
 */
var publish = function(sub, clientCollectionName, mapper,
                        start, end, dealershipId, user){
  if (user){
    var callback = function(results){
      results.forEach(function(result){
        sub.added(clientCollectionName, Random.id(), result.value);
      });
      sub.ready();
    }
    Aggregations.run(mapper, start, end, dealershipId, user, callback);
  }
};

Meteor.publish('carsReport', function(start, end, dealershipId, user){
  publish(this, "carsReport", "cars",
          start, end, dealershipId, user);
});

Meteor.publish('financeManagersReport', function(start, end, dealershipId, user){
  publish(this, "financeManagersReport", "financeManagers",
          start, end, dealershipId, user);
});

Meteor.publish('salespeopleReport', function(start, end, dealershipId, user){
  publish(this, "salespeopleReport", "salespeople",
          start, end, dealershipId, user);
});
