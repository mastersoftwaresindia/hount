Meteor.startup(function(){
  Migrations.migrateTo('latest');
});

// Embed answer documents on deals
Migrations.add({
  version: 1,
  up: function(){
    Answers.find().forEach(function(answer){
      var embed = _.omit(answer, '_id', 'dealId')
      Deals.update({_id: answer.dealId}, {
        $set: {
          answer: embed
        }
      });
    });
  }
});

// Migrate old roles to work with roles package
Migrations.add({
  version: 2,
  up: function(){
    Meteor.users.find().forEach(function(user){
      if (user.profile && user.profile.role){
        var role = user.profile.role;
        if (role === "director"){
          role = "director-finance"; // Default old directors to Finance Directors
        }
        Roles.setUserRoles(user._id, role);
        Meteor.users.update(user._id, {
          $unset: {
            "profile.role": ""
          }
        });
      }
    });
  }
});

// Create actual user accounts for salespeople
Migrations.add({
  version: 3,
  up: function(){
    Salespeople.find().forEach(function(salesperson){
      // Replace non a-z, A-Z, 0-9, _ with a dot in salespersons name and lower it
      var newUsername = salesperson.name.replace(/\W/g, ".").toLowerCase();

      // Extra stuff in here to possibly handle taken usernames (increment number as last char)
      var userId = null;
      var currentTry = 0;
      while(!userId){
        if (currentTry > 0){
          if (currentTry > 1){
            newUsername = newUsername.substring(0, newUsername.length - 1);
          }
          newUsername += currentTry;
        }
        try{
          userId = Accounts.createUser({
            username: newUsername,
            password: "letmein1",
            profile: {
              active: salesperson.active,
              name: salesperson.name
            }
          });
        } catch(ex){
          currentTry++;
          console.log(newUsername, "is in use, trying again...");
        }
      }
      console.log("Created", newUsername);
      Roles.setUserRoles(userId, "salesperson");

      // Update salespersonIds on past deals
      Deals.update({salespersonId: salesperson._id},
        { $set: { salespersonId: userId } },
        { multi: true}
      );
    });
  }
});


// Migrate answer fields to use proper data types
Migrations.add({
  version: 4,
  up: function(){
    Deals.find().forEach(function(deal){
      if (deal.answer){
        Deals.update({_id: deal._id}, {
          $set: {
            "answer.profit": Number(deal.answer.profit),
            "answer.financeProfit": Number(deal.answer.financeProfit),
            "answer.possession": deal.answer.possession === "yes" || deal.answer.possession === true
          }
        });
      }
    });
  }
});

// Create initial dealership and roll all existing users/deals/cars into that dealership
Migrations.add({
  version: 5,
  up: function(){

    // Create owner
    var ownerId = Accounts.createUser({
      username: "yarkowner",
      password: "letmein1",
      profile: {
        name: "Yark Owner",
        active: true
      }
    });

    Roles.setUserRoles(ownerId, "owner");

    // Create the new dealership
    var dealershipId = Dealerships.insert({
      name: "Yark",
      active: true,
      ownerId: ownerId
    });

    var update = {$set: {"dealershipId": dealershipId}},
        options = {multi: true}

    // Update all non-admins to belong to the new dealership
    Meteor.users.update({"roles": {$nin: ['admin', 'owner']}}, update, options);
    Cars.update({}, update, options);
    Deals.update({}, update, options);
  }
});
