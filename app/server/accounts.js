Accounts.config({
  forbidClientAccountCreation: false
});
// company registration

Meteor.methods({
  companyreg: function(userId, user, pw, role) {
    if (userId){
      // If user already exists - update it

      // Get the users current document
      var currentUser = Meteor.users.findOne(userId);
      // Extend current profile with new profile
      user.profile = _.extend(currentUser.profile, user.profile);
      // Update email
      user.emails = [{address: email, verified: false}];
      // Set em
      Meteor.users.update(userId, {$set: user});
      if (pw) {
        Accounts.setPassword(userId, pw);
      }
    } else {
      // The users is a new one - create it
      user.password = pw;
      userId = Accounts.createUser(user);
    }
     if (role){
      Roles.setUserRoles(userId, role);

      if (role === 'admin' || role === 'owner'){
        Meteor.users.update(userId, {$unset: { dealershipId: "" } });
      }
    }
  }  
  // updateOffset: function(offset){
  //   Meteor.users.update(Meteor.userId(), {$set: {"profile.offset": offset}});
  // }
});

Meteor.methods({
  upsertUser: function(userId, user, email, pw, dealershipId, role) {
    if (userId){
      // If user already exists - update it

      // Get the users current document
      var currentUser = Meteor.users.findOne(userId);
      // Extend current profile with new profile
      user.profile = _.extend(currentUser.profile, user.profile);
      // Update email
      user.emails = [{address: email, verified: false}];
      // Set em
      Meteor.users.update(userId, {$set: user});
      if (pw) {
        Accounts.setPassword(userId, pw);
      }
    } else {
      // The users is a new one - create it
      user.email = email;
      user.password = pw;
      userId = Accounts.createUser(user);
    }


    // Update dealershipId and roles
    dealershipId = dealershipId ? dealershipId : Meteor.user().dealershipId;
    if (dealershipId){
      Meteor.users.update(userId, {$set: { dealershipId: dealershipId } });
    }

    if (role){
      Roles.setUserRoles(userId, role);

      if (role === 'admin' || role === 'owner'){
        Meteor.users.update(userId, {$unset: { dealershipId: "" } });
      }
    }
  },

  updateReportSettings: function(settings){
    Meteor.users.update(Meteor.userId(), {$set: settings});
  }

  // updateOffset: function(offset){
  //   Meteor.users.update(Meteor.userId(), {$set: {"profile.offset": offset}});
  // }
});
Meteor.methods({
  upsertdeleteUser: function(userId) {
    if(userId){
      console.log('hellop');
      Meteor.users.remove({"_id":userId});
    }
  }
});


// Meteor.startup(function(){
//   var userId = Accounts.createUser({
//     username: "schnie",
//     email: "greg@differential.io",
//     password: "letmein1",
//     profile: {
//       name: "schnie",
//       active: true
//     }
//   });
//   Roles.setUserRoles(userId, 'admin');
// });
