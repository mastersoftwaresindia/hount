QueryBuilder = function(){};

QueryBuilder.getQueryForMyDealerships = function(user){
  var query = {};
  if (Roles.userIsInRole(user, ['owner'])){
    var dealershipIds = this.getOwnerDealerships(user);
    query.dealershipId = { $in: dealershipIds };
  } else if (!Roles.userIsInRole(user, ['admin'])){
    query.dealershipId = user.dealershipId;
  }
  return query;
};

QueryBuilder.getOwnerDealerships = function(user){
  return Dealerships.find({ 'ownerId': user._id }).map(function(dealership){
    return dealership._id;
  });
};

QueryBuilder.getQueryForCompletedDeals = function(user, start, end, dealershipId, name){
  var query = dealershipId
      ? { dealershipId: dealershipId }
      : QueryBuilder.getQueryForMyDealerships(user);

  query.status = 'completed';
  query.completedAt = {
    $gte: start,
    $lte: end
  };
  query.$or = [
    {
      customerFirstName: {
        $regex: name, $options: 'i'
      }
    },{
      customerLastName: {
        $regex: name, $options: 'i'
      }
    }
  ];

  return query;
};
