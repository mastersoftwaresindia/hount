/*
 * Map Reduce publications for the Reports.
 */

Aggregations = function(){};

Aggregations.maps = {
  cars: function(){
    var key = this.carsId;
    var value = {
      fk: key,
      count: 1,
      waitingTime: this.activeAt - this.createdAt,
      financeTime: this.completedAt - this.activeAt,
      totalTime: this.completedAt - this.createdAt,
      salesProfit: this.answer.profit || 0,
      financeProfit: this.answer.financeProfit || 0
    };
    emit(key, value);
  },

  financeManagers: function(){
    var key = this.financeManagerId;
    var value = {
      fk: key,
      count: 1,
      waitingTime: this.activeAt - this.createdAt,
      financeTime: this.completedAt - this.activeAt,
      totalTime: this.completedAt - this.createdAt,
      salesProfit: this.answer.profit || 0,
      financeProfit: this.answer.financeProfit || 0
    };
    emit(key, value);
  },

  salespeople: function(){
    var key = this.salespersonId;
    var value = {
      fk: key,
      count: 1,
      waitingTime: this.activeAt - this.createdAt,
      financeTime: this.completedAt - this.activeAt,
      totalTime: this.completedAt - this.createdAt,
      salesProfit: this.answer.profit || 0,
      financeProfit: this.answer.financeProfit || 0
    };
    emit(key, value);
  }
};

Aggregations.fnReduce = function(key, values){
  var reduced = {
    fk: key,
    count: values.length,
    waitingTime: 0,
    financeTime: 0,
    totalTime: 0,
    salesProfit: 0,
    financeProfit: 0
  };

  var waitingTimes = [], financeTimes = [], totalTimes = [],
      salesProfits = [], financeProfits = [];

  values.forEach(function(value){
      waitingTimes.push(value.waitingTime);
      financeTimes.push(value.financeTime);
      totalTimes.push(value.totalTime);
      salesProfits.push(value.salesProfit);
      financeProfits.push(value.financeProfit);
  });
  reduced.waitingTime = Array.sum(waitingTimes) / values.length;
  reduced.financeTime = Array.sum(financeTimes) / values.length;
  reduced.totalTime = Array.sum(totalTimes) / values.length; // calculate total average time on the basis of count 

  reduced.salesProfit = Array.sum(salesProfits) / values.length; // calculate the average sales profit
  reduced.financeProfit = Array.sum(financeProfits) / values.length; // calculate the finance profit

  return reduced;
};


Aggregations.fnAddDiscoStick = function(results){
  if (results.length > 0){
    var totals = {}, averages = {};
    var rows = _.pluck(results, "value");

    // Totals up each field on the result objects
    // and puts them into the totals array
    _.each(_.keys(rows[0]), function(key, i){
      if (key !== "fk"){
        totals[key] = _.reduce(_.pluck(rows, key), function(memo, num){
          return memo + parseInt(num);
        }, 0);
      }
    });

    // Calculates averages based on the totals and results count
    _.each(_.keys(totals), function(key, i){
      averages[key] = parseInt((totals[key] / results.length).toFixed(2));
    });

    // Push these two new "rows" to the final result
    // to be published
    results.push({_id: Random.id(), value: averages});
    results.push({_id: Random.id(), value: totals});
  }
  return results;
};

Aggregations.run = function(reportName, start, end, dealershipId, user, callback){

  var mapper = this.maps[reportName];

  var query = dealershipId
      ? { dealershipId: dealershipId }
      : QueryBuilder.getQueryForMyDealerships(user);

  query.status = "completed";
  query.completedAt = { $gte: start, $lte: end };

  var self = this;
  var preprocessor = Meteor.bindEnvironment(function(error, results){
    if (error){
      throw new Meteor.Error(500, error);
    } else {
      results = self.fnAddDiscoStick(results);
      callback(results);
    }
  }, function(error){
    console.error(error);
  });

  var db = MongoInternals.defaultRemoteCollectionDriver().mongo.db;
  db.collection("deals").mapReduce(mapper, this.fnReduce, {
    query: query,
    out: {
      // Delivers the response to the callback
      inline: 1,
      // This will output to this collection
      // reduce: "car-report-aggregations"
    }
  }, preprocessor);
}
