/* Setup nodemailer
 * TODO: move user/pass to settings.json and use Meteor.settings
 */
var nodemailer = Meteor.require("nodemailer");
var transport = nodemailer.createTransport("Mailgun",{
  auth: {
      user: "postmaster@cetrackingsystems.com",
      pass: "1erksss7fwp8"
  }
});


/*
 * Sends emails the given schedule ('day', 'week', 'month', etc)
 */
var sendEmailsFor = function(schedule){

  // Get US timezone where its 11:00pm
  var timezone = DateHelper.findTimezoneWhere(23);
  if (timezone){


    // Get start, end times for reporting correct timespans
    // If we are in UTC mode (server) we technically need to query for
    // "yesterday", since we would be ahead of the US
    var start, end;
    if (DateHelper.isInUTC()){
      var offset = timezone.offsets[DateHelper.isDST() ? "dst" : "std"];
      start = moment.utc().add('hours', offset).startOf(schedule).toDate();
      end = moment.utc().add('hours', offset).endOf(schedule).toDate();
    } else {
      start = moment().startOf(schedule).toDate();
      end = moment().endOf(schedule).toDate();
    }


    // Find the users on the given schedule and in the timezone
    var users = Meteor.users.find({
      "profile.reportSchedule": schedule,
      "profile.timezone": timezone.value
    });


    // Loop through the users and send appropriate emails
    users.forEach(function(user){
      if (user.emails && user.emails[0].address){

        // Query for deals this user has access to
        // and are between the specified start and end dates
        var query = QueryBuilder.getQueryForCompletedDeals(user, start, end, null, null);
        var completedDeals = Deals.find(query).fetch();

        // Convert json results to csv string
        var csv = DataHelper.getDealsCsv(completedDeals);

        // Send email with attachment
        transport.sendMail({
          from: "CE Tracking Systems <reports@cetrackingsystems.com>",
          to: user.emails[0].address,
          subject: "Reports",
          text: "Your reports are attached.",
          attachments: [{
            fileName: "deals.csv",
            contents: csv
          }]
        });

      }
    });

  }

};


/*
 * Schedules jobs for the Cron runner
 */
var schedule = function(scheduleStr, scheduleFn){
  SyncedCron.add({
    name: "Send emails (" + scheduleStr + ")",
    schedule: scheduleFn,
    job: function(){
      sendEmailsFor(scheduleStr);
    }
  });
};

/*
 * Schedule jobs to send emails then start the cron job manager
 */
Meteor.startup(function(){
  schedule("day", function(parser){
    // Run the first minute of every hour
    return parser.recur().first().minute();
    // return parser.recur().first().second(); // tester
  });

  schedule("week", function(parser){
    // Run the first minute of every hour on the last day of the week (sat)
    return parser.recur().first().minute().last().dayOfWeek();
  });

  schedule("month", function(parser){
    // Run the first minute of every hour on the last day of the month
    return parser.recur().first().minute().last().dayOfMonth();
  });

  SyncedCron.start();
});
