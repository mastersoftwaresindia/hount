/*
 * These are the collections that the publication will
 * push report documents into.
 */

CarsReport = new Meteor.Collection("carsReport");
FinanceManagersReport = new Meteor.Collection("financeManagersReport");
SalespeopleReport = new Meteor.Collection("salespeopleReport");
