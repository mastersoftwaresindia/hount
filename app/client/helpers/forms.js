/* jQuery extension to get values from form elements
 * Use $(form).objectify() to return an object with key val pairs
 */

$.fn.extend({
  getValue: function(name) {
    var $input = this.find("input[name='" + name + "'], select[name='" + name + "']");
    var type = $input.attr("type");

    if (type === "number"){
      return parseInt($input.val());
    } else if (type === "checkbox"){
      return $input.prop("checked");
    } else if (type ==="radio"){
      var val = null;
      $input.each(function(i, e){
        if ($(e).prop("checked")){
          val = $(e).val();
          return false;
        }
      });
      return val;
    } else {
      return $input.val();
    }
  },

  objectify: function(){
    var obj = {};
    var form = this;
    form.find("input[name], select[name]").each(function(i, e){
      var name = $(e).attr("name");
      obj[name] = form.getValue(name);
    });
    return obj;
  }
});
