UI.registerHelper("currentUserRole", function(){
  var user = Meteor.user();
  if (user){
    var inRole = function(role){
      return Roles.userIsInRole(user, role);
    };

    if (inRole("admin")){
      return "Administrator";
    } else if (inRole("owner")){
      return "Owner";
    } else if (inRole("director-finance")){
      return "Director / Manager";
    } else if (inRole("director-sales")){
      return "Director of Sales";
    } else if (inRole("manager")){
      return "Finance Manager";
    } else if (inRole("salesperson")){
      return "Salesperson";
    } else {
      return "Unknown Role";
    }
  }
});
