UI.body.rendered = function(){
  Meteor.setInterval(function(){
    $.each($('.time-since'), function(index, element){
      var time = moment($(element).data('time'));
      var offset = 1380 - time.zone();
      var timeSince = moment(moment(new Date()).diff(time)).subtract('minutes', offset).format('HH:mm:ss');
      $(element).text(timeSince);
    });
  }, 500);

  $("body").on("change", function(e){
      $el = $(e.target);
      group = $el.parents('.reactive-table-navigation').attr('reactive-table-group');
      if (group){
        Session.set(group + "-reactive-table-current-page", 0);
      }
  });
};
