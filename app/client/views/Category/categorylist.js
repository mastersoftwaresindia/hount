var fields = // set date in tabel 
  [{
    key: 'name',
    label: 'Category Name'
  }
  ,{
    key: 'active',
    label: 'Active',
    fn: function(val, obj){
      return val ? "Yes" : "No"
    }
  },{
    key: '_id',
    label: 'Edit ',
    tmpl: Template.editcategory
  },{
    key: 'Delete',
    label: 'Delete',
    tmpl: Template.deletecategory
  }];
  
  
/**
 * @mss to add manufacturer into database collections
 */
Template.categorieslist.events({
    'submit form': function(e){
      e.preventDefault();
      var form = $(e.target).objectify();
      if ( Categories.findOne({"name":form.category}) ) { // to check already existing manufacturer
          $('.categoryerror').text("Categorie Already Exist Please add another one !!!").show();
          $('.categoryerror').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
        var categoryid = this.Categorie ? this.Categorie._id : null;
        Meteor.call("upsertcategory", categoryid, form.category, form.active,
        function(error, response){
          if ( error && !response ){ // check if value inserted into database
            alert(error);
          } else {
            Router.go("categorieslist"); // redirect to manufaturer screen
            $(".categorysuess").text("Success, you have added Categorie successfully !!! ").show();
            $('.categorysuess').delay(1000).fadeOut(1000); // close error message in 1 second
          }
        });
      }
      $('#category').val('');
    },
});

Template.categoryDelete.events({ // Delete manufacturer
  'submit form':function(){
    var categoryid = this.Categorie ? this.Categorie._id : null;
   Meteor.call("categorydelete", categoryid,
      function(error, response){
        if (error){
          alert(error);
        } else {
          Router.go("categorieslist");
        }
    });
    return false;
  },
  'click #categorycancel':function(){
      Router.go("categorieslist");// redirect to manufaturer screen
    }
});

Template.CategorieEdit.events({
  'submit form': function(e){
    e.preventDefault();
    var form = $(e.target).objectify();
     if ( Categories.findOne({"name":form.category}) ) { // to check already existing manufacturer
          $('.categoryerror').text("Categorie Already Exist Please add another one !!!").show();
          $('.categoryerror').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
          var categoryid = this.Categorie ? this.Categorie._id : null;
          Meteor.call("upsertcategory", categoryid, form.category, form.active,
            function(error, response){
              if (error){
                alert(error);
              } else {
                Router.go("categorieslist");
              }
          });
         }
  },
  'click #categorycancel':function(){
    Router.go("categorieslist");
  }
});

Template.categorieslist.helpers({
  tableSettings: function(){
    return {
      rowsPerPage: 10,
      showFilter: true,
      showNavigation: 'auto',
      group: 'categories-table',
      fields: fields
    };
  }
})