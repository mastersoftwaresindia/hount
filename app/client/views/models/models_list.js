var fields = // set date in tabel 
  [{
    key: 'name',
    label: 'Model Name'
  },{
    key:'manfacturer',
    label:'Manfacturer'
  }
  ,{
    key: 'active',
    label: 'Active',
    fn: function(val, obj){
      return val ? "Yes" : "No"
    }
  },{
    key: '_id',
    label: 'Edit ',
    tmpl: Template.editmodel
  },{
    key: 'Delete',
    label: 'Delete',
    tmpl: Template.deletemodel
  }];

/**
 * @mss to add Model into database collections
 */
Template.modelsList.events({
    'submit form': function(e){
      e.preventDefault();
      var form = $(e.target).objectify();
      if ( model.findOne({"name":form.modelname}) ) { // to check already existing model
          $('.modelerrmsg').text("model Already Exist Please add another one !!!").show();
          $('.modelerrmsg').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
        var modelid = this.model ? this.model._id : null;
        Meteor.call("upsertmodel", modelid, form.modelname,form.manufactur, form.modelactive,
        function(error, response){
          if ( error && !response ){ // check if value inserted into database
            alert(error);
          } else {
            Router.go("modelsList"); // redirect to model screen
            $(".modelsussess").text("Success, you have added manufacturer successfully !!! ").show();
            $('.modelsussess').delay(1000).fadeOut(1000); // close error message in 1 second
          }
        });
      }
      $('#manufact').val('');
    },
});

Template.modelsDelete.events({ // Delete model
  'submit form':function(){
      var modelid = this.model ? this.model._id : null;
   Meteor.call("modeldelete", modelid,
      function(error, response){
        if (error){
          alert(error);
        } else {
          Router.go("modelsList");
        }
    });
    return false;
  },
  'click #manfacrcancel':function(){
      Router.go("modelsList");// redirect to manufaturer screen
    }
});

Template.modelsList.helpers({
  tableSettings: function(){
    return {
      rowsPerPage: 10,
      showFilter: true,
      showNavigation: 'auto',
      group: 'model-table',
      fields: fields
    };
  }
})