
Template.modelsEdit.events({ // edit model
  'submit form': function(e){
    e.preventDefault();
    var form = $(e.target).objectify();
    if ( model.findOne({"name":form.modelname,"manfacturer":form.manufactur}) ) { // check model already exist or not
          $('.modelerrmsg').text("model Already Exist Please add another one !!!").show();
          $('.modelerrmsg').delay(1000).fadeOut(1000); // close error message in 1 second
    }else{
        var modelid = this.model ? this.model._id : null;
        Meteor.call("upsertmodel", modelid, form.modelname,form.manufactur, form.active, // edit model
          function(error, response){
            if (error){
              alert(error);
            } else {
              Router.go("modelsList");
            }
        });
      }
  },
    'click #cancelmodel':function(){
         Router.go("modelsList");
    }
});

Template.roleRadio.helpers({
  inRole: function(user, role){
    return Roles.userIsInRole(user, role);
  }
});
