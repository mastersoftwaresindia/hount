Template.userEdit.rendered = function(){
  if (!this.data.user){
    var $el = $("input[type=radio]").eq(0);
    $el.prop("checked", "checked");
    Session.set('userEdit.role', $el.val());
  } else {
    var myRoles = Roles.getRolesForUser(this.data.user);
    if (myRoles.length > 0){
      Session.set('userEdit.role', myRoles[0]);
    }
  }
};


Template.userEdit.helpers({
  editingSelf: function(){
    return this.user && this.user._id === Meteor.userId();
  },
  hideDealership: function(){
    // Hide dealership if we are editing ourself,
    // if the user being edited is an admin or owner
    // or if the seleted role is an admin or owner
    var selectedRole = Session.get('userEdit.role');
    return this.user && this.user._id === Meteor.userId()
            || selectedRole === 'admin'
            || selectedRole === 'owner';
  },
  isDealership: function(selectedDealership){
    return this._id === selectedDealership;
  },
  usersEmail: function(){
    if (this.user && this.user.emails){
      return this.user.emails[0].address;
    }
  }
});

Template.userEdit.events({
  'change [name=role]': function(e){
    Session.set('userEdit.role', $(e.target).val());
  },
  'submit form': function(e){
    e.preventDefault();

    var form = $(e.target).objectify();

    var user = {
      username: form.username,
      profile: {
        active: form.active,
        name: form.name
      }
    };

    var userId = this.user ? this.user._id : null;
    Meteor.call("upsertUser", userId, user, form.email, form.password, form.dealership, form.role,
      function(error, response){
        if (error){
          alert(error);
        } else {
          Router.go("userList");
        }
    });
  }
});
Template.userDelete.events({ // Delete manufacturer
  'submit form':function(){
    var userId = this.user ? this.user._id : null;
    //alert(userId);
    Meteor.call("upsertdeleteUser", userId,
      function(error, response){
        if (error){
          alert(error);
        } else {
          Router.go("userList");
        }
    });
    return false;
  },
  'click #usercancel':function(){
      Router.go("userList");// redirect to manufaturer screen
    }
});
Template.roleRadio.helpers({
  inRole: function(user, role){
    return Roles.userIsInRole(user, role);
  }
});
