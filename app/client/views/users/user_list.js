var fields =
  [{
    key: 'username',
    label: 'Username'
  },{
    key: 'emails.0.address',
    label: 'Email'
  },{
    key: '_id',
    label: 'type',
    fn: function(val, obj){
      var inRole = function(role){
        return Roles.userIsInRole(val, role);
      };

      if (inRole("admin")){
        return "Administrator";
      } else if (inRole("owner")){
        return "Owner";
      } else if (inRole("user")){
        return "simpleuser";
      } else {
        return "Unknown Role";
      }
    }
  },{
    key: 'profile.active',
    label: 'Active',
    fn: function(val, obj){
      return val ? "Yes" : "No"
    }
  },{
    Key:'_id',
    label:'Delete',
    tmpl:Template.deleteusers
    },{
    key: '_id',
    label: 'Edit',
    tmpl: Template.editUserButton
  }];
  isValidPassword = function(password) {  
    if (password.length < 6) {
        usereroor='Your password should be 6 characters or longer.';
        return false;
    }
    return true;
};

areValidPasswords = function(password, confirm) {  
    if (!isValidPassword(password)) {
        return false;
    }
    if (password !== confirm) {
        usereroor='Your two passwords are not equivalent.';
        return false;
    }
    return true;
};

Template.userList.events({     // add  new user 
  'change [name=role]': function(e){
    Session.set('userEdit.role', $(e.target).val());
  },
  'submit form': function(e){
    e.preventDefault();
    var form = $(e.target).objectify();
    var user = {
      username: form.username,
      profile: {
        active: form.active,
        name: form.name,
        company:form.company,
      }
    };
    if (user.profile.company===form.company) {
     $('.usererror').text("Something went wrong").show();
    }else{
          var userId = this.user ? this.user._id : null;
          if (form.admin==true) { // check to assign user role
            var userrole='admin';
          }
          else{
            var userrole='user';
          }
         if(isValidPassword(form.password) && areValidPasswords(form.password, form.canfirmPassword)) {
            Meteor.call("upsertUser", userId, user, form.email, form.password,form.dealership, userrole, //add new user
                 function(error, response){
                   if (error){
                     alert(error);
                   } else {
                     Router.go("userList");
                     $('.usersucess').text('successfully added').show();
                       $('.usersucess').delay(1000).fadeOut(1000); // close error msd hide
                   }
            });
         }
         else{
            $('.usererror').text(usereroor).show();
              $('.usererror').delay(1000).fadeOut(1000); // close error msd hide
             }
        }
    }
  });

Template.userList.helpers({
  tableSettings: function(){
    return {
      rowsPerPage: 10,
      showFilter: true,
      showNavigation: 'auto',
      group: 'users-table',
      fields: fields
    };
  }
})
