
Template.manufacturerEdit.events({
  'submit form': function(e){
    e.preventDefault();
    var form = $(e.target).objectify();
     if ( Manufactur.findOne({"name":form.manufact}) ) { // to check already existing manufacturer
          $('.manfacrterrer').text("Manufacturer Already Exist Please add another one !!!").show();
          $('.manfacrterrer').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
          var manufactid = this.manufact ? this.manufact._id : null;
          Meteor.call("upsertManufact", manufactid, form.manufact, form.active,
            function(error, response){
              if (error){
                alert(error);
              } else {
                Router.go("manufacturerList");
              }
          });
         }
  },
  'click #cancelmanufacturer':function(){
    Router.go("manufacturerList");
  }
});

Template.roleRadio.helpers({
  inRole: function(user, role){
    return Roles.userIsInRole(user, role);
  }
});
