var fields = // set date in tabel 
  [{
    key: 'name',
    label: 'Manufacturer Name'
  }
  ,{
    key: 'active',
    label: 'Active',
    fn: function(val, obj){
      return val ? "Yes" : "No"
    }
  },{
    key: '_id',
    label: 'Edit ',
    tmpl: Template.editmanufacturer
  },{
    key: 'Delete',
    label: 'Delete',
    tmpl: Template.deletemanufacturer
  }];

/**
 * @mss to add manufacturer into database collections
 */
Template.manufacturerList.events({
    'submit form': function(e){
      e.preventDefault();
      var form = $(e.target).objectify();
      if ( Manufactur.findOne({"name":form.manufact}) ) { // to check already existing manufacturer
          $('.manfacrterrer').text("Manufacturer Already Exist Please add another one !!!").show();
          $('.manfacrterrer').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
        var manufactid = this.manufact ? this.manufact._id : null;
        Meteor.call("upsertManufact", manufactid, form.manufact, form.active,
        function(error, response){
          if ( error && !response ){ // check if value inserted into database
            alert(error);
          } else {
            Router.go("manufacturerList"); // redirect to manufaturer screen
            $(".alert-success").text("Success, you have added manufacturer successfully !!! ").show();
            $('.alert-success').delay(1000).fadeOut(1000); // close error message in 1 second
          }
        });
      }
      $('#manufact').val('');
    },
});

Template.manufacturerDelete.events({ // Delete manufacturer
  'submit form':function(){
    var manufactid = this.manufact ? this.manufact._id : null;
    //alert(manufactid);
    Meteor.call("manufacterdelete", manufactid,
      function(error, response){
        if (error){
          alert(error);
        } else {
          Router.go("manufacturerList");
        }
    });
    return false;
  },
  'click #manfacrcancel':function(){
      Router.go("manufacturerList");// redirect to manufaturer screen
    }
});

Template.manufacturerList.helpers({
  tableSettings: function(){
    return {
      rowsPerPage: 10,
      showFilter: true,
      showNavigation: 'auto',
      group: 'manufacturer-table',
      fields: fields
    };
  }
})