Template.header.events({
    'click #logout':function(event){
       Meteor.logout(function(error) {
      if (error) {
        alert(error);
        // Display the logout error to the user however you want
      }
      else{
         Router.go("/");
      }
    });
    }
});