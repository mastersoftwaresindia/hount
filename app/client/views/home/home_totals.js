avgTimeBetween = function(start, end){
  var total = 0;
  _.each(this.completedDeals, function(deal) {
    total += moment(deal[end]).diff(deal[start]);
  });
  var count = this.completedDeals.length || 1;

  var time = moment(new Date());
  var offset = 1380 - time.zone();
  return moment(total/count).subtract('minutes', offset).format('HH:mm:ss');
};

avgCalculator = function(fn) {
  var count = this.completedDeals.length;
  var profit = 0;

  _.each(this.completedDeals, function(deal){
    profit += fn(deal.answer) || 0;
  }, this);

  return accounting.formatMoney(profit/count);
};

totalCalculator = function(fn){
  var profit = 0;

  _.each(this.completedDeals, function(deal){
    profit += fn(deal.answer) || 0;
  }, this);

  return accounting.formatMoney(profit);
};

Template.homeTotals.helpers({
  dealCount: function() {
    return this.completedDeals.length;
  },
  avgTimeWithFinance: function(){
    return avgTimeBetween.call(this, 'activeAt', 'completedAt');
  },
  avgTimeInWaitingRoom: function(){
    return avgTimeBetween.call(this, 'createdAt', 'activeAt');
  },
  avgTotalTime: function(){
    return avgTimeBetween.call(this, 'createdAt', 'completedAt');
  },
  avgTotalProfit: function() {
    return avgCalculator.call(this, function(answer){
      return parseInt(answer.financeProfit);
    });
  },
  avgTotalSalesProfit: function() {
    return avgCalculator.call(this, function(answer){
      return parseInt(answer.profit);
    });
  },
  avgAllProfit: function() {
    return avgCalculator.call(this, function(answer){
      return parseInt(answer.profit) + parseInt(answer.financeProfit);
    });
  },
  totalSalesProfit: function() {
    return totalCalculator.call(this, function(answer){
      return parseInt(answer.profit);
    });
  },
  totalAllProfit: function() {
    return totalCalculator.call(this, function(answer){
      return parseInt(answer.profit) + parseInt(answer.financeProfit);
    });
  },
  totalProfit: function() {
    return totalCalculator.call(this, function(answer){
      return parseInt(answer.financeProfit);
    });
  }
});
