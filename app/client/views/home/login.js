// register company
//validation check
isValidPassword = function(password) {  
    if (password.length < 6) {
        usereroor='Your password should be 6 characters or longer.';
        return false;
    }
    return true;
};

areValidPasswords = function(password, confirm) {  
    if (!isValidPassword(password)) {
        return false;
    }
    if (password !== confirm) {
        usereroor='Your two passwords are not equivalent.';
        return false;
    }
    return true;
};
Template.companyregister.events({
    'change [name=role]': function(e){
    Session.set('userEdit.role', $(e.target).val());
  },
  'submit form': function(e){
    e.preventDefault();

    var form = $(e.target).objectify();

    var user = {
      username: form.username,
      profile: {
        active: 'true',
        name: form.username,
        company:form.company,
        address:form.address,
        address2:form.address2,
        zipcode:form.zipcode,
        siteurl:form.siteurl
      }
    };
    var userId = this.user ? this.user._id : null;
     if(isValidPassword(form.password) && areValidPasswords(form.password, form.re_enter_password)) {
        Meteor.call("companyreg", userId, user, form.password, 'owner', //add new user
             function(error, response){
               if (error){
                 alert(error);
               } else {
                 Router.go("userList");
                 $('.usersucess').text('successfully Create Your Account Please Login Your account').show();
               }
        });
     }
     else{
        $('.usererror').text(usereroor).show();
          $('.usererror').delay(1000).toggle(3000); // close error msd hide
    }
  }
    
    });
//Login account
Template.loginuser.events({
      'change [name=role]': function(e){
    Session.set('userEdit.role', $(e.target).val());
  },
  'submit form': function(e){
    e.preventDefault();

    var form = $(e.target).objectify();
    Meteor.loginWithPassword(
      form.company,
      form.password,
      function(error) {
        if (error) {
           $('.loginerror').text(error).show();
          $('.loginerror').delay(2000).toggle(2000); // close error msd hide
           // Display the login error to the user however you want
        }
        else{
          //return Meteor.userId() && Meteor.user() && Meteor.user().profile ? Meteor.user().profile.company : "";
        Router.go("/");
        }
      
      }
    );
    }
});
