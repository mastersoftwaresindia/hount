var fields = // set date in tabel 
  [{
    key: 'name',
    label: 'Power Soures Name'
  }
  ,{
    key: 'active',
    label: 'Active',
    fn: function(val, obj){
      return val ? "Yes" : "No"
    }
  },{
    key: '_id',
    label: 'Edit ',
    tmpl: Template.editPowersources
  },{
    key: 'Delete',
    label: 'Delete',
    tmpl: Template.deletePowersources
  }];
  
  
/**
 * @mss to add manufacturer into database collections
 */
Template.Powersourceslist.events({
    'submit form': function(e){
      e.preventDefault();
      var form = $(e.target).objectify();
      if ( Powersources.findOne({"name":form.powersoures}) ) { // to check already existing manufacturer
          $('.powererror').text("Power Soures Already Exist Please add another one !!!").show();
          $('.powererror').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
       var Powersourcesid = this.Powersources ? this.Powersources._id : null;
        Meteor.call("upsertpower", Powersourcesid, form.powersoures, form.active,
        function(error, response){
          if ( error && !response ){ // check if value inserted into database
            alert(error);
          } else {
            Router.go("Powersourceslist"); // redirect to manufaturer screen
            $(".powersuess").text("Success, you have added Categorie successfully !!! ").show();
            $('.powersuess').delay(1000).fadeOut(1000); // close error message in 1 second
          }
        });
      }
      $('#category').val('');
    },
});

Template.PowersourcesDelete.events({ // Delete manufacturer
  'submit form':function(){
    var Powersourcesid = this.Powersources ? this.Powersource._id : null;
   // alert(Powersourcesid);
   Meteor.call("powerdelete", Powersourcesid,
      function(error, response){
        if (error){
          alert(error);
        } else {
          Router.go("Powersourceslist");
        }
    });
    return false;
  },
  'click #powercancel':function(){
      Router.go("Powersourceslist");// redirect to manufaturer screen
    }
});

Template.PowersourcesEdit.events({
  'submit form': function(e){
    e.preventDefault();
    var form = $(e.target).objectify();
   // alert(form.powersoures);
     if ( Powersources.findOne({"name":form.powersoures}) ) { // to check already existing manufacturer
          $('.powererror').text("Power Soures Already Exist Please add another one !!!").show();
          $('.powererror').delay(1000).fadeOut(1000); // close error message in 1 second
      } else{
          var Powersourcesid = this.Powersources ? this.Powersource._id : null;
          Meteor.call("upsertpower", Powersourcesid, form.powersoures, form.active,
            function(error, response){
              if (error){
                alert(error);
              } else {
                Router.go("Powersourceslist");
              }
          });
         }
  },
  'click #powercancel':function(){
    Router.go("Powersourceslist");
  }
});

Template.Powersourceslist.helpers({
  tableSettings: function(){
    return {
      rowsPerPage: 10,
      showFilter: true,
      showNavigation: 'auto',
      group: 'categories-table',
      fields: fields
    };
  }
})