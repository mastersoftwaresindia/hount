/*
 * GLOBAL Configurations
 */
Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

// Forces IR to wait on subscriptions
// Router.onBeforeAction('loading');

// Check to make sure user is logged in

var goToDashboard = function(pause) {
  if (Meteor.user()) {
    Router.go('homePage');
    pause();
  }
};
Router.onBeforeAction(goToDashboard, {only: ['splash']});

if(Meteor.isClient){
  Router.onBeforeAction(function (pause) {
    if (!this.ready()) {
      this.render('loading');
      pause();
    }
    if (Meteor.loggingIn()){
      this.render('loading');
      pause();
    }
    if (!Meteor.user()){
      this.render('signIn');
      pause();
    }   
  });
}


Router.map(function() {
  /*
   * HOME Routes
   */
  this.route('homePage', {  // home router
    path: '/',
    waitOn: function(){
      var start = new Date(moment().startOf('day').format());
      var end = new Date(moment().endOf('day').format());
      return [
        Meteor.subscribe('completedDealsForDay', start, end),
        Meteor.subscribe('dealsByStatus', ['waiting', 'active']),
        Meteor.subscribe('allModels'),
        Meteor.subscribe('users'),
        Meteor.subscribe('allCategories'),
        Meteor.subscribe('allManufacturer')];
    },
    data: function(){
      return{
        completedDeals: Deals.find({status: 'completed'}).fetch(),
        waitingDeals: Deals.find({status: {$in: ['waiting']}}, {limit: 5}).fetch(),
        activeDeals: Deals.find({status: {$in: ['active']}}, {limit: 5}).fetch(),
        salespeople: Meteor.users.find({'roles': {$in: ['salesperson']}}).fetch(),
        financeManagers: Meteor.users.find({'roles': {$in: ['manager']}}).fetch(),
          user: Meteor.users.findOne(this.params._id),
      //  cars: Cars.find().fetch()
      };
    }
  });

  /*
   * manufacturer Routes
   */
  this.route('manufacturerList', { // manfacturer router defined
    path: '/manufacturer', // create path
    waitOn: function(){
      return Meteor.subscribe('allManufacturer');
    },
    data: function(){
      return {
        manufacturer: Manufactur.find().fetch() // get all manufacturer data
      };
    }
  });
  
  this.route('manufacturerDelete',{ // delete route create 
    path:'/manufacturer/:_id/delete',  // path id add in url 
     waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('activeDealerships'));
      }
      subs.push(Meteor.subscribe('manufacturerById', this.params._id)); // find  manufacturer id 
      return subs;
    },
      data: function() {
      return {
        manufact: Manufactur.findOne(this.params._id),
        manufacturer: Manufactur.find().fetch()
      };
    }
    });
  this.route('manufacturerEdit', { // edit manfacturer router
    path: '/manufacturer/:_id/edit',  // redirect to id path
    waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('activeDealerships'));
      }
      subs.push(Meteor.subscribe('manufacturerById', this.params._id));
      return subs;
    },
    data: function() {
      return {
        manufact: Manufactur.findOne(this.params._id),
        manufacturer: Manufactur.find().fetch()
      };
    }
  });
    /*
   * modelsList Routes
   */
  this.route('modelsList', { // model router defined 
    path: '/models',  // model path create 
    waitOn: function(){
      return [
              Meteor.subscribe('allModels'),
              Meteor.subscribe('allManufacturer')
              ]; // subscriber defined
    },
    data: function(){
      return {
         model : model.find().fetch(),
        manufacturer: Manufactur.find().fetch() // fetch all model data in database 
      };
    }
  });
  
  this.route('modelsDelete',{ // model delete router 
    path:'/models/:_id/delete', // model id pass in url 
     waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('activeDealerships'));
      }
      subs.push(Meteor.subscribe('modelById', this.params._id));
      return subs;
    },
      data: function() {
      return {
        model: model.findOne(this.params._id),
        models: model.find().fetch() // get all model data 
      };
    }
    });
  
  this.route('modelsEdit', { // model edit router 
    path: '/models/:_id/edit', // pass id in url in m model
    waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('activeDealerships'));
      }
      subs.push(Meteor.subscribe('modelById', this.params._id));
      return [subs,
               Meteor.subscribe('allManufacturer')];
    },
    data: function() {
      return {
         model: model.findOne(this.params._id),
         models : model.find().fetch(), // fetch all model data in database
          manufacturer: Manufactur.find().fetch()
      };
    }
  });
    /*
   * categorieslist Routes
   */
    this.route('categorieslist', {
    path: '/categories',
    waitOn: function(){
      return Meteor.subscribe('allCategories');
    },
    data: function(){
      return {
        categories: Categories.find().fetch()
      };
    }
  });
    /*
     *category Edit 
     */
      this.route('CategorieEdit', { // category edit router 
    path: '/categories/:_id/edit', // pass id in url in category
    waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('activeDealerships'));
      }
      subs.push(Meteor.subscribe('categoryById', this.params._id));
      return [subs,
               Meteor.subscribe('allCategories')];
    },
    data: function() {
      return {
         Categorie: Categories.findOne(this.params._id),
         Categories : Categories.find().fetch(), // fetch all category data in database
      };
    }
  });
      /*
       *Delete Categorie
       */
        this.route('categoryDelete',{ // category delete router 
    path:'/categories/:_id/delete', // category id pass in url 
     waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('allCategories'));
      }
      subs.push(Meteor.subscribe('categoryById', this.params._id));
      return subs;
    },
      data: function() {
      return {
          Categorie: Categories.findOne(this.params._id),
         Categories : Categories.find().fetch(), // fetch all category data in database
      };
    }
    });
     /*
   * Power and Resouress Routes
   */
    this.route('Powersourceslist', {
    path: '/power',
    waitOn: function(){
      return Meteor.subscribe('allPowersources');
    },
    data: function(){
      return {
        Powersources: Powersources.find().fetch()
      };
    }
  });
     /*
     *category Edit 
     */
      this.route('PowersourcesEdit', { // category edit router 
    path: '/power/:_id/edit', // pass id in url in category
    waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('activeDealerships'));
      }
      subs.push(Meteor.subscribe('PowerById', this.params._id));
      return [subs,
               Meteor.subscribe('allPowersources')];
    },
    data: function() {
      return {
         Powersource: Powersources.findOne(this.params._id),
         Powersources : Powersources.find().fetch(), // fetch all category data in database
      };
    }
  });
      /*
       *Delete Categorie
       */
        this.route('PowersourcesDelete',{ // category delete router 
    path:'/power/:_id/delete', // category id pass in url 
     waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('allPowersources'));
      }
      subs.push(Meteor.subscribe('PowerById', this.params._id));
      return subs;
    },
      data: function() {
      return {
        Powersource: Powersources.findOne(this.params._id),
         Powersources : Powersources.find().fetch(), // fetch all category data in database
      };
    }
    });
   /*
   * Inventorylist Routes
   */
    this.route('Inventorylist', {
    path: '/inventory',
    waitOn: function(){
      return Meteor.subscribe('allInventorys');
    },
    data: function(){
      return {
        Inventorys: Inventorys.find().fetch()
      };
    }
  });
/*
 * Profile
 */
this.route('Profile', {
    path: '/profile',
    data: function(){
      return {
       profile:Meteor.users.find().fetch()
      };
    }
  });
 
  // USER Routes
  UsersController = RouteController.extend({
    onBeforeAction: function (pause) {
      if (!Roles.userIsInRole(Meteor.user(), ['admin', 'owner', 'director-finance', 'director-sales'])){
        this.render('accessDenied');
        pause();
      }
    }
  });

  this.route('userList', {
    path: '/users',
    controller: 'UsersController',
    waitOn: function(){
      return Meteor.subscribe('users');
    },
    data: function(){
      return {
        users: Meteor.users.find().fetch(),
        };
    }
  });
this.route('userDelete', {
    path: '/user/:_id/delete',
      waitOn: function(){
      var subs = [];
      if (Roles.userIsInRole(Meteor.user(), ['admin', 'owner'])){
        subs.push(Meteor.subscribe('allCategories'));
      }
      subs.push(Meteor.subscribe('userById', this.params._id));
      return subs;
    },
    data: function(){
      return {
        user: Meteor.users.findOne(this.params._id),
      };
    }
  });


  this.route('userEdit', {
    path: '/user/:_id/edit',
    controller: 'UsersController',
    template: 'userEdit',
    waitOn: function(){
      return [
        Meteor.subscribe('userById', this.params._id),
        Meteor.subscribe('activeDealerships')];
    },
    data: function(){
      return {
        user: Meteor.users.findOne(this.params._id)
      };
    }
  });



  // REPORT Routes
  this.route('carReportList', {
    path: '/reports/cars',
    waitOn: function(){
      return [
        Meteor.subscribe('carsReport',
          Session.get('report.start'),
          Session.get('report.end'),
          Session.get("report.dealershipId"),
          Meteor.user()),
        Meteor.subscribe('allCars'),
        Meteor.subscribe('allDealerships')];
    },
    data: function(){
      return {
        stats: CarsReport.find().fetch(),
        dealerships: Dealerships.find().fetch()
      };
    }
  });

  this.route('dealReportList', {
    path: '/reports/deals/',
    waitOn: function(){
      return [
        Meteor.subscribe('completedDealsSearch',
          Session.get('report.start'),
          Session.get('report.end'),
          Session.get("report.dealershipId"),
          null),
        Meteor.subscribe('activeFinanceManagers'),
        Meteor.subscribe('activeCars'),
        Meteor.subscribe('activeSalespeople'),
        Meteor.subscribe('allDealerships')];
    },
    data: function(){
      return {
        cars: Cars.find().fetch(),
        deals: Deals.find().fetch(),
        salespeople: Meteor.users.find({'roles': {$in: ['salesperson']}}).fetch(),
        financeManagers: Meteor.users.find({'roles': {$in: ['manager']}}).fetch(),
        dealerships: Dealerships.find().fetch()
      };
    }
  });

  this.route('financeReportList', {
    path: '/reports/finance-managers',
    waitOn: function(){
      return [
        Meteor.subscribe('financeManagersReport',
          Session.get('report.start'),
          Session.get('report.end'),
          Session.get("report.dealershipId"),
          Meteor.user()),
        Meteor.subscribe('allFinanceManagers'),
        Meteor.subscribe('allDealerships')];
    },
    data: function(){
      return {
        stats: FinanceManagersReport.find().fetch(),
        dealerships: Dealerships.find().fetch()
      };
    }
  });

  this.route('salespersonReportList', {
    path: '/reports/salespeople',
    waitOn: function(){
      return [
        Meteor.subscribe('salespeopleReport',
          Session.get('report.start'),
          Session.get('report.end'),
          Session.get("report.dealershipId"),
          Meteor.user()),
        Meteor.subscribe('allSalespeople'),
        Meteor.subscribe('allDealerships')];
    },
    data: function(){
      return {
        stats: SalespeopleReport.find().fetch(),
        dealerships: Dealerships.find().fetch()
      };
    }
  });

  this.route('reportSettings', {
    path: '/reports/settings'
  });

  // this.route('reportsExport', {
  //   where: 'server',
  //   path: '/reports/:report/export',
  //   action: function(){
  //     var params = this.params;
  //     params.start = moment(params.start, "MM-DD-YYYY").toDate();
  //     params.end = moment(params.end, "MM-DD-YYYY").toDate();
  //     // console.log(this.params);
  //
  //     var Future = Meteor.require('fibers/future');
  //     var fut = new Future();
  //
  //     var user = Meteor.users.findOne(params.userId);
  //     Aggregations.run(params.report, params.start, params.end, params.dealershipId, user, function(json){
  //       fut.return(json);
  //     });
  //
  //     console.log(fut.wait());
  //
  //     var json2csv = Meteor.require('json2csv');
  //     var convert = Async.wrap(json2csv);
  //     var csv = "";
  //     try{
  //       // csv = convert({data: json, fields: ['car', 'price', 'color']});
  //     } catch(e){
  //       console.error(e);
  //     }
  //
  //     this.response.writeHead(200, {
  //       'Content-Type': 'application/csv',
  //       'Content-Disposition': 'attachment; filename=report.csv',
  //       'Pragma': 'no-cache',
  //       'Expires': '0'
  //     });
  //
  //     this.response.end(csv);
  //   }
  // });

});
