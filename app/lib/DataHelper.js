DataHelper = function(){};

DataHelper.formatDuration = function(val){
  return moment.duration(val, 'milliseconds').format("h:mm:ss", {trim: false});
};

DataHelper.formatDate = function(val){
  return moment(val).format("MM/DD/YYYY");
};

DataHelper.formatCar = function(id){
  var car = Cars.findOne(id);
  if (car){
    return car.name;
  }
};

DataHelper.formatDealership = function(id){
  var dealership = Dealerships.findOne(id);
  if (dealership){
    return dealership.name;
  }
};

DataHelper.formatUser = function(id){
  var user = Meteor.users.findOne(id);
  if (user){
    return user.profile.name;
  }
};

DataHelper.getDuration = function(end, start){
  return moment.utc(moment(end, "DD/MM/YYYY HH:mm:ss").diff(moment(start,"DD/MM/YYYY HH:mm:ss")))
};

DataHelper.getDealsDiscoStick = function(deals){
  var waitingTimes = [], financeTimes = [], totalTimes = [],
      salesProfits = [], financeProfits = [];

  _.each(deals, function(e, i){
    waitingTimes.push(e.activeAt - e.createdAt),
    financeTimes.push(e.completedAt - e.activeAt),
    totalTimes.push(e.completedAt - e.createdAt),
    salesProfits.push(e.answer.profit || 0),
    financeProfits.push(e.answer.financeProfit || 0)
  });

  var sum = function(arr){
    return _.reduce(arr, function(memo, num){
      return memo + parseInt(num);
    }, 0);
  };

  var avg = function(arr){
    return sum(arr) / deals.length;
  };

  var averages = {
    _id: Random.id(),
    // completedAt: this.deals[0].completedAt,
    // carsId: this.deals[0].carsId,
    waitingTime: avg(waitingTimes),
    financeTime: avg(financeTimes),
    totalTime: avg(totalTimes),
    salesProfit: avg(salesProfits),
    financeProfit: avg(financeProfits)
  };

  var totals = {
    _id: Random.id(),
    // completedAt: this.deals[0].completedAt,
    // carsId: this.deals[0].carsId,
    waitingTime: sum(waitingTimes),
    financeTime: sum(financeTimes),
    totalTime: sum(totalTimes),
    salesProfit: sum(salesProfits),
    financeProfit: sum(financeProfits)
  };

  return [averages, totals];
};

DataHelper.getDealsCsv = function(deals){
  var self = this;

  // Get the discostick
  var discoStick = self.getDealsDiscoStick(deals);
  discoStick = _.map(discoStick, function(row){
    return {
      "_id": "", "customerFirstName": "", "customerLastName": "", "status": "",
      "Car": "", "Salesperson": "", "Finance Manager": "",
      "Dealership": "",
      "waitingTime": self.formatDuration(row.waitingTime),
      "financeTime": self.formatDuration(row.financeTime),
      "totalTime": self.formatDuration(row.totalTime),
      "profit": accounting.formatMoney(row.salesProfit),
      "financeProfit": accounting.formatMoney(row.financeProfit)
    }
  });

  // Massage the deal data
  var stats = _.map(deals, function(deal){
    var row = deal;

    // Format ids to human readable
    row["Car"] = self.formatCar(row.carsId);
    delete row.carsId;

    row["Salesperson"] = self.formatUser(row.salespersonId);
    delete row.salespersonId;

    row["Finance Manager"] = self.formatUser(row.financeManagerId);
    delete row.financeManagerId;

    row["Dealership"] = self.formatDealership(row.dealershipId);
    delete row.dealershipId;

    delete row.lastModified;


    // Calculate durations
    row.waitingTime = self.formatDuration( self.getDuration(row.activeAt, row.createdAt) );
    row.financeTime = self.formatDuration( self.getDuration(row.completedAt, row.activeAt) );
    row.totalTime = self.formatDuration( self.getDuration(row.completedAt, row.createdAt) );
    delete row.createdAt;
    delete row.activeAt;
    delete row.completedAt;


    // Flatten answer to main object
    // row.possession = row.answer.possession;
    row.profit = accounting.formatMoney(row.answer.profit);
    row.financeProfit = accounting.formatMoney(row.answer.financeProfit);
    delete row.answer;

    return row;
  });

  stats = stats.concat(discoStick);
  return json2csv(stats, true, true);
};
