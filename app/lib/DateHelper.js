DateHelper = function(){};

// Returns second sunday in march this year.
DateHelper.DSTstart = function(){
  var date = moment().month(2).date(1);
  var daysTilSecondSunday = 7 + (7 - date.day());
  date.add('days', daysTilSecondSunday);
  date.set('hour', 2);
  return date;
};

// Returns first sunday in november this year.
DateHelper.DSTend = function(){
  var date = moment().month(10).date(1);
  var daysTilFirstSunday = 7 - date.day();
  date.add('days', daysTilFirstSunday);
  date.set('hour', 2);
  return date;
};

// Is the US in DST now?
DateHelper.isDST = function(){
  return moment().isAfter(this.DSTstart()) && moment().isBefore(this.DSTend());
};

// Is server in UTC mode? (used to iron out differences between dev and prod)
DateHelper.isInUTC = function(){
  return moment().zone() === 0;
};


// Get the timezone where it is {hour}
DateHelper.findTimezoneWhere = function(hour){
  var offsetKey = this.isDST() ? "dst" : "std";
  var timezone;
  this.getTimezones().forEach(function(tz){
    var offset = tz.offsets[offsetKey];
    if (moment.utc().add('hours', offset).hour() === hour){
      timezone = tz;
    }
  });
  return timezone;
};

// List of US Timezones
DateHelper.getTimezones = function(){
  return [{
    name: "Eastern Time",
    value: "est",
    offsets: {
      std: -5,
      dst: -4
    }
  }, {
    name: "Central Time",
    value: "cst",
    offsets: {
      std: -6,
      dst: -5
    }
  }, {
    name: "Mountain Time",
    value: "mst",
    offsets: {
      std: -7,
      dst: -6
    }
  }, {
    name: "Pacific Time",
    value: "pst",
    offsets: {
      std: -8,
      dst: -7
    }
  }]
};
